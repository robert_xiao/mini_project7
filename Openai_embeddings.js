let fetch;

async function loadDependencies() {
    if (!fetch) {
        fetch = (await import('node-fetch')).default;
    }
}
const openAiHeaders = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer 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`
};


async function createEmbedding(textToEmbed){
    await loadDependencies(); 
    
    let response = await fetch('https://api.openai.com/v1/embeddings', {
        method: 'POST',
        headers: openAiHeaders,
        body: JSON.stringify({
            "input": textToEmbed,
            "model": "text-embedding-3-small"
        }),
    });
    if (response.ok){
        const data = await response.json();
        const embeddingArray = data.data[0].embedding;
        console.log(embeddingArray);
        saveArrayAsFile(embeddingArray, 'mini_project7/embeddingArray.txt');
        console.log(data);
        return data;

    } else {
        console.error('Failed to fetch embedding:', response.statusText);
    }
}
const fs = require('fs');
function saveArrayAsFile(array, filename) {
    const text = array.join('\n');
    fs.writeFile(filename, text, (err) => {
        if (err) throw err;
        console.log('File saved successfully!');
    });
}

// createEmbedding('Hello World'); 
// createEmbedding('Cats and Dogs'); 
// createEmbedding('Nice to meet you'); 
createEmbedding('Cats');






