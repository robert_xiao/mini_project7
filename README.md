# mini_project7

## Overview:
Based on OpenAI's API, text data is converted into vectors using the text-embedding-3-small text embedding model, and the text-vector data pairs are saved in the database using singlestore, which facilitates subsequent querying and merging.<br>


## Requirements
Ingest data into Vector database<br>
Perform queries and aggregations<br>
Visualize output<br>

## Steps:
1.	Get OpenAI's API secret key first.<br>
 ![Alt text](image-9.png)
2.	Based on the following code, the text data is converted into vector data using OpenAI's text-embedding-3-small model and the vector data is saved in a text file called embeddingArray. In total, three text data are converted into vector data, Hello World, Cats and Dogs, Nice to meet you.<br>
 
![Alt text](image-8.png)<br>
 ![Alt text](image-7.png)<br>
3.	The above code runs as follows.<br>
 ![Alt text](image-6.png)<br>
4.	Put text-vector data pairs into database, store vector data as BLOB type in database.<br>
 ![Alt text](image-5.png)<br>
5.	Use single store to query data, if I want to search Cats and Dogs text data, then I want to use Cats text data corresponding to the vector data as the query input, based on the calculation of the vector's cosine similarity, so as to get the text query results under different scores.<br>
1)	First get the vector data corresponding to the Cats text data.<br>
 ![Alt text](image-4.png)<br>
 ![Alt text](image-3.png)<br>
2)	The following code is given to query the data.<br>
 ![Alt text](image-2.png)
3)	The vector data corresponding to the Cats text data is used as query input, and the query is performed using single store to output the text data in the vector database under different scores.<br>
![Alt text](image.png)
4)	Visualize data results.<br>
 ![Alt text](image-1.png)

